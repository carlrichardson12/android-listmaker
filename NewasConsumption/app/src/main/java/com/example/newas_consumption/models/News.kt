package com.example.newas_consumption.models

abstract class NewsStuff

data class News(val id: Int, val author: String?, val title: String?,
                val description: String?, val url: String?,
                val urlToImage: String?, val publishedAt: String?,
                val content: String?)

data class source(val id: String?, val name: String?, val news: News )