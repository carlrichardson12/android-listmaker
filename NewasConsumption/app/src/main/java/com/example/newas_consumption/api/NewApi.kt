package com.example.newas_consumption.api

import android.provider.SyncStateContract
import com.example.newas_consumption.models.News
import io.reactivex.Observable
//
import retrofit2.Response
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url


interface NewsApi {
    @GET("/v2/{endpoint}")
    fun getNews(@Path("endpoint") endpoint: String): Response<News>
   // fun getNews(@Path("https://newsapi.org/v2/everything?q=bitcoin&from=2020-01-10&sortBy=publishedAt&apiKey=74959f6045024b69a0b2c803607611b3")): Observable<News>
//
    companion object Factory {
        fun create(): NewsApi {
            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://newsapi.org/v2/").build()

            return retrofit.create(NewsApi::class.java)
        }
    }
}