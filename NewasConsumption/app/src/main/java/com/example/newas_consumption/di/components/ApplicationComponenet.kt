package com.example.newas_consumption.di.components
import com.example.newas_consumption.BaseApp
import com.example.newas_consumption.di.module.ApplicationModule
import dagger.Component

/**
 * Created by ogulcan on 07/02/2018.
 */
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(application: BaseApp)

}