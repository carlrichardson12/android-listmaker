package com.example.newas_consumption.presentation.list

import com.example.newas_consumption.api.NewsApi
import com.example.newas_consumption.models.News
import com.example.newas_consumption.models.NewsViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList
import io.reactivex.functions.Function3
import io.reactivex.android.schedulers.AndroidSchedulers
import java.lang.Exception

class ListPresenter: ListContract.Presenter {
    private val subs = ArrayList<ArrayList<News>>()
    private val newApi: NewsApi = NewsApi.create()
    private lateinit var view: ListContract.View

    //applys the subscre
    override fun subscribe() {

    }

    override fun unsubscribe() {
        subs.clear()
    }

    //attaches veriables o r data to view??
    override fun attach(view: ListContract.View) {
        this.view = view
    }


    //getting the news making the request
    override fun loadData() {
//        var subscribe = newApi.getNews().subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({ list: Any? -> getObject(list.toString())
//                view.showProgress(false)
//            }, { error ->
//                view.showProgress(false)
//                view.showErrorMessage("App is broken pimp")
//            })
        //changed it to an array list
        try {
            var subscribe =
                newApi.getNews("everything?q=bitcoin&from=2020-01-10&sortBy=publishedAt&apiKey=74959f6045024b69a0b2c803607611b3")
                    .body()
           var getArry =  this.getObject(subscribe.toString())
            subs.add(getArry)
        } catch (e: Exception){
            println(e)
            println("something went wrong")
        }


    }

     override fun mockData(): ArrayList<List<News>> {
        val list = ArrayList<List<News>>()
       val dList =  listOf(News(1,null, "Type 'Should I' into Google and you'll see why Tesla's stock was surging",
            "Traders are turning to the Google for advice on how to invest in Tesla’s (TSLA) extraordinary share-price run-up, in a testament to the broadening speculation surrounding the stock.",
            "https://finance.yahoo.com/news/google-should-i-why-teslas-stock-is-surging-150553927.html",
            "https://s.yimg.com/uu/api/res/1.2/ZFAHtWc4_Myo9Gt0nM1ngg--~B/aD0xMDA0O3c9MTQ2MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-02/4d50bcd0-4828-11ea-9ffd-c14d9430035a",
            "2020-02-05T15:05:53Z","Tesla (TSLA) stock trading advice is trending on Google, in a testament to the broadening speculation surrounding the stock after its extraordinary run-up.\r\nAs of Wednesday morning, typing Should I into Google (GOOG, GOOGL) produced a set of top suggested sea… [+3935 chars]"),
            News(2, null, "Type 'Should I' into Google and you'll see why Tesla's stock was surging",
                "Traders are turning to the Google for advice on how to invest in Tesla’s (TSLA) extraordinary share-price run-up, in a testament to the broadening speculation surrounding the stock.",
                "https://finance.yahoo.com/news/google-should-i-why-teslas-stock-is-surging-150553927.html",
                "https://s.yimg.com/uu/api/res/1.2/ZFAHtWc4_Myo9Gt0nM1ngg--~B/aD0xMDA0O3c9MTQ2MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-02/4d50bcd0-4828-11ea-9ffd-c14d9430035a",
                "2020-02-05T15:05:53Z","Tesla (TSLA) stock trading advice is trending on Google, in a testament to the broadening speculation surrounding the stock after its extraordinary run-up.\r\nAs of Wednesday morning, typing Should I into Google (GOOG, GOOGL) produced a set of top suggested sea… [+3935 chars]"),
            News(3,null, "Type 'Should I' into Google and you'll see why Tesla's stock was surging",
                "Traders are turning to the Google for advice on how to invest in Tesla’s (TSLA) extraordinary share-price run-up, in a testament to the broadening speculation surrounding the stock.",
                "https://finance.yahoo.com/news/google-should-i-why-teslas-stock-is-surging-150553927.html",
                "https://s.yimg.com/uu/api/res/1.2/ZFAHtWc4_Myo9Gt0nM1ngg--~B/aD0xMDA0O3c9MTQ2MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-02/4d50bcd0-4828-11ea-9ffd-c14d9430035a",
                "2020-02-05T15:05:53Z","Tesla (TSLA) stock trading advice is trending on Google, in a testament to the broadening speculation surrounding the stock after its extraordinary run-up.\r\nAs of Wednesday morning, typing Should I into Google (GOOG, GOOGL) produced a set of top suggested sea… [+3935 chars]"))
         list.add(dList)
        return list
    }


    //Gets the News Object
    private  fun getObject(Tson: String): ArrayList<News> {
        val jsonList = JSONArray(Tson)
        val listNews = ArrayList<News>()
        var i = 0

        while (i < jsonList.length()) {
            val newsObj = jsonList.getJSONObject(i)
            listNews.add(
                News(
                    (1..15).shuffled().first(),
                    newsObj.getString("author"),
                    newsObj.getString("title"),
                    newsObj.getString("description"),
                    newsObj.getString("url"),
                    newsObj.getString("urlToImage"),
                    newsObj.getString("publishedAt"),
                    newsObj.getString("content")
                )
            )
            i++
        }
        return listNews
    }
}