package com.example.newas_consumption.di.module

import android.app.Application
import com.example.newas_consumption.BaseApp
import com.example.newas_consumption.di.scope.NewsScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val baseApp: BaseApp) {

    @Provides
    @Singleton
    @NewsScope
    fun provideApplication(): Application {
        return baseApp
    }
}