package com.example.newas_consumption.presentation.main

import com.example.newas_consumption.presentation.base.BaseContract


class MainContract {

    interface View: BaseContract.View {
        fun showListFragment()
    }

    interface Presenter: BaseContract.Presenter<View> {
        fun onDrawerOptionAboutClick()
    }
}