package com.example.newas_consumption.di.module

import android.app.Activity
import com.example.newas_consumption.presentation.main.MainContract
import com.example.newas_consumption.presentation.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class NewsModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter()
    }

}