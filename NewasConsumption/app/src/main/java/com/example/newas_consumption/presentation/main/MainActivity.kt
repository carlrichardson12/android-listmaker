package com.example.newas_consumption.presentation.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import com.example.newas_consumption.R
import com.example.newas_consumption.di.components.DaggerActivityComponent
import com.example.newas_consumption.di.module.NewsModule
import com.example.newas_consumption.presentation.list.ListFragment
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject lateinit var present: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //call all depenancies
        injectDependency()

        present.attach(this)
    }



        // injecting activity dependacy
    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder().newsModule(NewsModule(this))
            .build()

        activityComponent.inject(this)
    }

    //shows the list fragments
    override fun showListFragment() {
        supportFragmentManager.beginTransaction()
            .disallowAddToBackStack()
            .setCustomAnimations(AnimType.SLIDE.getAnimPair().first, AnimType.SLIDE.getAnimPair().second)
            .replace(R.id.frame, ListFragment().newInstance(), ListFragment.TAG)
            .commit()
    }

    /**
     * this section handles the selection and processing of the differnt menus to see
     * on the screen
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    //styles for changing fragements
    enum class AnimType {
        SLIDE,
        FADE;

        fun getAnimPair(): Pair<Int, Int> {
            when(this) {
                SLIDE -> return Pair(R.layout.left, R.layout.right)
                FADE -> return Pair(R.layout.fadein, R.layout.fadeout)
            }

            return Pair(R.layout.left, R.layout.right)
        }
    }
}


//    override fun onResume() {
//        super.onResume()
//        test()
//    }

//    private fun test() {
//        hello.setText("Hello world with kotlin extensions")
//    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        return when (item.itemId) {
//            R.id.action_settings -> true
//            else -> super.onOptionsItemSelected(item)
//        }
//    }
//        setSupportActionBar(toolbar)
//        // for little mail thing
//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }
//   override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        when(item!!.itemId) {
//                R.id.nav_item_info -> {
//                present.onDrawerOptionAboutClick()
//                return true
//            }
//            else -> {
//
//            }
//        }
//
//        return super.onOptionsItemSelected(item)
//    }
//
//    override fun onBackPressed() {
//        val fragmentManager = supportFragmentManager
//        val fragment = fragmentManager.findFragmentByTag(ListFragment.TAG)
//
//        if (fragment == null) {
//            super.onBackPressed()
//        } else {
//            supportFragmentManager.popBackStack()
//        }
//    }