package com.example.newas_consumption.di.components

import com.example.newas_consumption.presentation.main.MainActivity
import com.example.newas_consumption.di.module.NewsModule
import dagger.Component

@Component(modules = arrayOf(NewsModule::class))
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)

}