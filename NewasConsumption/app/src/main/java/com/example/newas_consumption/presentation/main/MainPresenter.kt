package com.example.newas_consumption.presentation.main
import io.reactivex.disposables.CompositeDisposable
class MainPresenter: MainContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var view: MainContract.View

    // this adds the compent
    override fun subscribe() {

    }

        //removes the main activity
    override fun unsubscribe() {
        subscriptions.clear()
    }

            // creats the views
    override fun attach(view: MainContract.View) {
        this.view = view
        view.showListFragment() // as default
    }

             //this shows the other frags
    override fun onDrawerOptionAboutClick() {
        view.showListFragment()
    }
}