package com.example.newas_consumption.di.components
import com.example.newas_consumption.di.module.FragmentModule
import com.example.newas_consumption.presentation.list.ListFragment
import dagger.Component

@Component(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {


    fun inject(listFragment: ListFragment)

}