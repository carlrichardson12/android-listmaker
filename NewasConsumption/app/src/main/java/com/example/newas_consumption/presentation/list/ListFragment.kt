package com.example.newas_consumption.presentation.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newas_consumption.R
import com.example.newas_consumption.di.components.DaggerFragmentComponent
import com.example.newas_consumption.di.module.FragmentModule
import com.example.newas_consumption.models.News
import kotlinx.android.synthetic.main.fragment_newslist.*
import javax.inject.Inject


class ListFragment: Fragment(), ListContract.View, ListAdapter.onItemClickListener {

    @Inject
    lateinit var presenter: ListContract.Presenter

    private lateinit var rootView: View

    fun newInstance(): ListFragment {
        return ListFragment()
    }
        // Operations that happen when list fragment triggers
    // and injects all nessary dependacees
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

   //has rendered and makes is
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater!!.inflate(R.layout.fragment_newslist, container, false)
        var  recyclerView = rootView.findViewById<RecyclerView>(R.id.recyclerView).setLayoutManager( LinearLayoutManager(this.context))
       loadDataSuccess(this.mockData())
        return rootView
    }
        //occurs when the view  created
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
        initView()
    }

        // desproyes ther view
    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

// shows the json when loaded and binds to text viw to recycler view
    override fun loadDataSuccess(list: List<News>) {
    var adapter = ListAdapter(activity, this.mockData().toMutableList() ,this)
    //figure out why this recycler view isnt working
    recyclerView.setAdapter(adapter)
    }

//     fun loadDataSuccess(recyclerView: Unit) {
//        var adapter = ListAdapter(activity, list.toMutableList(),this)
//        var adapter = ListAdapter(activity, this.mockData() ,this)
//        recyclerView.setAdapter(adapter)
//    }

        // shows loading progress
    override fun showProgress(show: Boolean) {
//        if (show) {
//            progressBar.visibility = View.VISIBLE
//        } else {
//            progressBar.visibility = View.GONE
//        }
    }




    // errror msg to display
    override fun showErrorMessage(error: String) {
        Log.e("error on the frag", error)
    }

    // intializing instancess
    private fun injectDependency() {
        val listComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .build()

        listComponent.inject(this)
    }

    private fun initView() {
        this.mockData()
       // presenter.loadData()
        presenter.mockData()
    }

    companion object {
        val TAG: String = "ListFragment"
    }

    override fun itemRemoveClick(post: News) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun itemDetail(postId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


     fun mockData(): List<News> {
        val list = List<News>(listOf(News(1,null, "Type 'Should I' into Google and you'll see why Tesla's stock was surging",
            "Traders are turning to the Google for advice on how to invest in Tesla’s (TSLA) extraordinary share-price run-up, in a testament to the broadening speculation surrounding the stock.",
            "https://finance.yahoo.com/news/google-should-i-why-teslas-stock-is-surging-150553927.html",
            "https://s.yimg.com/uu/api/res/1.2/ZFAHtWc4_Myo9Gt0nM1ngg--~B/aD0xMDA0O3c9MTQ2MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-02/4d50bcd0-4828-11ea-9ffd-c14d9430035a",
            "2020-02-05T15:05:53Z","Tesla (TSLA) stock trading advice is trending on Google, in a testament to the broadening speculation surrounding the stock after its extraordinary run-up.\r\nAs of Wednesday morning, typing Should I into Google (GOOG, GOOGL) produced a set of top suggested sea… [+3935 chars]"),
            News(2, null, "Type 'Should I' into Google and you'll see why Tesla's stock was surging",
                "Traders are turning to the Google for advice on how to invest in Tesla’s (TSLA) extraordinary share-price run-up, in a testament to the broadening speculation surrounding the stock.",
                "https://finance.yahoo.com/news/google-should-i-why-teslas-stock-is-surging-150553927.html",
                "https://s.yimg.com/uu/api/res/1.2/ZFAHtWc4_Myo9Gt0nM1ngg--~B/aD0xMDA0O3c9MTQ2MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-02/4d50bcd0-4828-11ea-9ffd-c14d9430035a",
                "2020-02-05T15:05:53Z","Tesla (TSLA) stock trading advice is trending on Google, in a testament to the broadening speculation surrounding the stock after its extraordinary run-up.\r\nAs of Wednesday morning, typing Should I into Google (GOOG, GOOGL) produced a set of top suggested sea… [+3935 chars]"),
            News(3,null, "Type 'Should I' into Google and you'll see why Tesla's stock was surging",
                "Traders are turning to the Google for advice on how to invest in Tesla’s (TSLA) extraordinary share-price run-up, in a testament to the broadening speculation surrounding the stock.",
                "https://finance.yahoo.com/news/google-should-i-why-teslas-stock-is-surging-150553927.html",
                "https://s.yimg.com/uu/api/res/1.2/ZFAHtWc4_Myo9Gt0nM1ngg--~B/aD0xMDA0O3c9MTQ2MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-02/4d50bcd0-4828-11ea-9ffd-c14d9430035a",
                "2020-02-05T15:05:53Z","Tesla (TSLA) stock trading advice is trending on Google, in a testament to the broadening speculation surrounding the stock after its extraordinary run-up.\r\nAs of Wednesday morning, typing Should I into Google (GOOG, GOOGL) produced a set of top suggested sea… [+3935 chars]"))
         )
         list.add(dList)
        return dList
    }
}