package com.example.newas_consumption.models

import com.google.gson.Gson

data class NewsViewModel(val json: String) {

    fun toJson(): String {
        return Gson().toJson(this)
    }
}