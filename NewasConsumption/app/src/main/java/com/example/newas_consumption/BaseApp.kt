package com.example.newas_consumption

import android.app.Application
import com.example.newas_consumption.di.components.ApplicationComponent
import com.example.newas_consumption.di.components.DaggerApplicationComponent
import com.example.newas_consumption.di.module.ApplicationModule

class BaseApp: Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        setup()

//        if (BuildConfig.DEBUG) {
//            // Maybe TimberPlant etc.
//        }
    }

    fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: BaseApp private set
    }
}