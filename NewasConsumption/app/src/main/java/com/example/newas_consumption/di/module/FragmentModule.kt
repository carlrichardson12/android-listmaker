package com.example.newas_consumption.di.module
import com.example.newas_consumption.api.NewsApi
import com.example.newas_consumption.presentation.list.ListContract
import com.example.newas_consumption.presentation.list.ListPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by ogulcan on 07/02/2018.
 */
@Module
class FragmentModule {


    @Provides
    fun provideListPresenter(): ListContract.Presenter {
        return ListPresenter()
    }

    @Provides
    fun provideApiService(): NewsApi {
        return NewsApi.create()
    }
}