package com.example.newas_consumption.presentation.list


import com.example.newas_consumption.models.News
import com.example.newas_consumption.presentation.base.BaseContract

class ListContract {

    interface View: BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun loadDataSuccess(list: List <News>)
    }

    interface Presenter: BaseContract.Presenter<View> {
        fun loadData()
        fun mockData():ArrayList<List<News>>
    }
}